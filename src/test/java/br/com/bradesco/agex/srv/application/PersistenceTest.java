package br.com.bradesco.agex.srv.application;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.bradesco.agex.srv.application.model.Pessoa;
import br.com.bradesco.agex.srv.application.util.JPAUtil;

public class PersistenceTest {

    @SuppressWarnings("unchecked")
	public static void main(String[] args) {

        Pessoa pessoa = new Pessoa();
//      pessoa.setId(2L);
        pessoa.setNome("Diego");
        
        EntityManager em = new JPAUtil().getEntityManager();
        
        em.getTransaction().begin();
        em.persist(pessoa);
        em.getTransaction().commit();
        
        
        em.getTransaction().begin();
        Query q = em.createNativeQuery("SELECT p.id, p.nome FROM Pessoa p", Pessoa.class);
        List<Pessoa> pessoas = q.getResultList();
        for (Pessoa p : pessoas) {
        	System.out.println(p);			
		}
        em.close();
    }
}
