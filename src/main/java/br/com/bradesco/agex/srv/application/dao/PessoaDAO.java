package br.com.bradesco.agex.srv.application.dao;

import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Default;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

import br.com.bradesco.agex.srv.application.model.Pessoa;

@Named
@Dependent
public class PessoaDAO {

	@Inject
	@Default //para injetar o EntityManager Default(que acessa o BD da app)
	private EntityManager em;

	public void save(Pessoa pessoa) {
		em.getTransaction().begin();
		em.persist(pessoa);
		em.getTransaction().commit();
		em.close();
	}

}
