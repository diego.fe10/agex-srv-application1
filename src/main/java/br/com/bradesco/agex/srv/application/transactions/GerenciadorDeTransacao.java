package br.com.bradesco.agex.srv.application.transactions;

import java.io.Serializable;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import br.com.bradesco.agex.srv.application.configuration.annotations.Transactional;

@Transactional
@Interceptor
public class GerenciadorDeTransacao implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private EntityManager em;
	private EntityTransaction transaction;
	
	@AroundInvoke
	public Object gerenciaTransacao(InvocationContext context) throws Exception {

		Object resultado = null;

		try {
			transaction = em.getTransaction();

			transaction.begin();
			
			resultado = context.proceed();
			
			transaction.commit();
		} catch (Exception e) {
			if(transaction != null && transaction.isActive()){
				transaction.rollback();
			}
			throw e;
		}
		return resultado;
	}

}
