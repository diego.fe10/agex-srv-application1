package br.com.bradesco.agex.srv.application.service.impl;

import javax.persistence.EntityManager;

import br.com.bradesco.agex.srv.application.model.Pessoa;
import br.com.bradesco.agex.srv.application.service.ProspectService;
import br.com.bradesco.agex.srv.application.util.JPAUtil;

public class ProspectServiceImpl implements ProspectService {
	
	@Override
	public void criar(Pessoa pessoa) {
        EntityManager em = new JPAUtil().getEntityManager();
        em.getTransaction().begin();
        em.persist(pessoa);
        em.getTransaction().commit();
        em.close();
	}

}
