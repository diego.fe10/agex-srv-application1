package br.com.bradesco.agex.srv.application.dao;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;

import br.com.bradesco.agex.srv.application.util.JPAUtil;

public class DAO<T> {

	private final Class<T> classe;
	
	@Inject
	private EntityManager em;
	
	public DAO(EntityManager manager, Class<T> classe) {
		this.em = manager;
		this.classe = classe;
	}

	public void adiciona(T t) {
		em.getTransaction().begin();
		em.persist(t);
		em.getTransaction().commit();
		new JPAUtil().close(em);
	}

	public void remove(T t) {
		// persiste o objeto
		em.remove(em.merge(t));
	}

	public void atualiza(T t) {
		// persiste o objeto
		em.merge(t);
	}

	public List<T> listaTodos() {
		CriteriaQuery<T> query = em.getCriteriaBuilder().createQuery(classe);
		query.select(query.from(classe));

		List<T> lista = em.createQuery(query).getResultList();

		return lista;
	}

	public T buscaPorId(Integer id) {
		T instancia = em.find(classe, id);
		return instancia;
	}
	
}
