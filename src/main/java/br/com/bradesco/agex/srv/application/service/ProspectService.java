package br.com.bradesco.agex.srv.application.service;

import br.com.bradesco.agex.srv.application.model.Pessoa;

public interface ProspectService {
	
	void criar(Pessoa pessoa);
}
