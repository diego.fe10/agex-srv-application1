package br.com.bradesco.agex.srv.application.resource;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.bradesco.agex.srv.application.configuration.annotations.Transactional;
import br.com.bradesco.agex.srv.application.dao.impl.ProspectDAO;
import br.com.bradesco.agex.srv.application.model.Pessoa;
import br.com.bradesco.agex.srv.application.util.JPAUtil;

@Path("/prospect")
public class ProspectResource {
	
	
	@Inject
	private ProspectDAO prospectDAO;
	
	@POST
	@Transactional
	public void criar(Pessoa pessoa) {
		prospectDAO.save(pessoa);
	}
	
	@Path("/lista-pessoa")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Pessoa buscaPessoa() {
		
		EntityManager em = new JPAUtil().getEntityManager();
           
        em.getTransaction().begin();
        Query q = em.createNativeQuery("SELECT p.id, p.nome FROM Pessoa p", Pessoa.class);
        Pessoa pessoa = (Pessoa) q.getSingleResult();
		em.close();
		return pessoa;
	}
	
}
