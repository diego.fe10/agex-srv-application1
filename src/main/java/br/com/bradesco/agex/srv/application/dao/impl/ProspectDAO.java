package br.com.bradesco.agex.srv.application.dao.impl;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.bradesco.agex.srv.application.configuration.annotations.Log;
import br.com.bradesco.agex.srv.application.dao.DAO;
import br.com.bradesco.agex.srv.application.model.Pessoa;

public class ProspectDAO  {
	
	@Inject
	private EntityManager em;
	private DAO<Pessoa> dao;
	
	@PostConstruct
	public void init() {
		this.dao = new DAO<Pessoa>(this.em, Pessoa.class);
	}
	
	@Log
	public void save(Pessoa pessoa) {
        dao.adiciona(pessoa);
	}
	
}
